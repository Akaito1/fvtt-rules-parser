rules-parser
============

Adds buttons to dnd5e item sheets to turn rules text into dice formula, etc. for items, spells, and features.
Intended use is copy-pasting out of a homebrew PDF from Dungeon Masters Guild or elsewhere.
Don't expect perfection, but this should save you some time.


How to use
----------

In Foundry VTT, use this module installation URL:
https://gitlab.com/Akaito1/fvtt-rules-parser/-/raw/master/module.json


TODO
----

- Replace flat bonus text in dice formula with "@mod" for attacks.
- Parse Spellcasting for actor's level and ability.
- Support localized rules text.

