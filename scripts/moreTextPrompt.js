// moreTextPrompt.js
import * as actorUpdater from './actorUpdater.js';
import * as itemUpdater from './itemUpdater.js';

export class MoreTextPrompt extends Application {
    constructor (options) {
        super(options);

        this.app = null;
        this.itemData = null;
        this.actorData = null;
    }


    activateListeners (html) {
        $('.rules-parser-parse-more').on('click', async function (event) {
            let text = $(event.target).parent().find('textarea')[0].value;
            if (this.actorData !== null) {
                actorUpdater.update(this.app, this.actorData, text, false);
            }
            else if (this.itemData !== null) {
                itemUpdater.update(this.app, this.itemData, text, false);
            }
            this.close();
        }.bind(this));  // otherwise 'this' is the DOM element, not the Application
    }


    setApp (app) {
        this.app = app;
    }

    setActorData (actorData) {
        this.actorData = actorData;
    }

    setItemData (itemData) {
        this.itemData = itemData;
    }


    static get defaultOptions () {
        return mergeObject(super.defaultOptions, {
            width: 400,
            height: 500,
            resizable: true,
            title: 'Rules Parser - More Text',
            template: '/modules/rules-parser/templates/more-text.hbs',
            classes: ['rulesparser', game.system.id],
            tabs: [],
        });
    }
}
