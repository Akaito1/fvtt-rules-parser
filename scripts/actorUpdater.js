// updater.js
import * as parser from './actorParser.js';

export async function update (app, data, text = null, allowGuesswork = false) {
    // ask parser for details
    console.log(app);
    console.log(data);
    let actor = app.object;
    let updates = {'data': parser.parse(actor, data.data, text, allowGuesswork)};
    console.log('parsed updates:'); console.log(updates);

    await actor.update(updates);
}
