const SKILL_MAP = {
    'acrobatics': 'acr',
    'animal handling': 'ani',
    'arcana': 'arc',
    'athletics': 'ath',
    'deception': 'dec',
    'history': 'his',
    'insight': 'ins',
    'intimidation': 'itm',
    'investigation': 'inv',
    'medicine': 'med',
    'nature': 'nat',
    'perception': 'prc',
    'performance': 'prf',
    'persuasion': 'per',
    'religion': 'rel',
    'sleight of hand': 'slt',
    'stealth': 'ste',
    'survival': 'sur',
};

const ABILITY_SKILL_MAP = {
    'acr': 'dex',
    'ani': 'wis',
    'arc': 'int',
    'ath': 'str',
    'dec': 'cha',
    'his': 'int',
    'ins': 'wis',
    'itm': 'cha',
    'inv': 'int',
    'med': 'wis',
    'nat': 'int',
    'prc': 'wis',
    'prf': 'cha',
    'per': 'cha',
    'rel': 'int',
    'slt': 'dex',
    'ste': 'dex',
    'sur': 'wis',
};


// see https://rpg.stackexchange.com/questions/161150/what-is-the-mathematical-formula-for-proficiency-bonus-vs-level-cr
function profFromCr (cr) {
    if (cr == 0)
        return 2;
    return Math.ceil(1 + cr / 4);
}


function modifierFromAbilityScore (score) {
    return Math.floor((score - 10) / 2);
}


// TODO: Support localized text in regex searches.


const AC_PATTERN_RE = /armor\s*class\s*(?<ac>\d+)/ims;

function parseArmorClass (text) {
    let match = null;
    if (match = AC_PATTERN_RE.exec(text)) {
        // belongs in 'attributes' object
        return {
            'ac': {
                'value': Number(match.groups['ac'])
            }
        };
    }
    return {};
}


const HP_PATTERN_RE = /hit\s*points\s*(?<hp>\d+)\s*\((?<dice>\d+d\d+(\s*[+-]\s*\d+)?)\)/ims;

function parseHitPoints (text) {
    let match = HP_PATTERN_RE.exec(text);
    if (!match)
        return {};

    // belongs in 'attributes' object
    return {
        'hp': {
            'value': Number(match.groups['hp']),
            'min': 0,
            'max': Number(match.groups['hp']),
            'formula': match.groups['dice'].replace(/\s+/imsg, ' '),
        }
    };
}


const SPEED_PATTERN_RE = /speed\s*(?<amount>\d+)\s*(?<unit>\w+\.?)(\s*,\s*(?<typeSpecial>\w+)\s*(?<amountSpecial>\d+)\s*(?<unitSpecial>\w+\.?))?/ims;

function parseSpeed (text) {
    let match = SPEED_PATTERN_RE.exec(text);
    if (!match)
        return {};
    // belongs in 'attributes' object
    let result = {
        'speed': {
            'value': `${match.groups['amount']} ${match.groups['unit']}`,
        }
    };

    if (match.groups['typeSpecial']) {
        result['speed']['special'] = 
            `${match.groups['typeSpecial']} ${match.groups['amountSpecial']} ${match.groups['unitSpecial']}`;
    }

    return result;
}


// Note inclusion of bad not-really-a-minus-sign symbol.
// If we were using the ability mod groups, we'd want to replace that with a real minus sign before returning it.
const ABILITY_SCORE_WITH_MOD_RE_STR = String.raw`str\s*(?<str>\d+)\s*\(\s*(?<strmod>[+−-]\d+)\s*\)`;
const ABILITY_SCORE_LINE_RE = new RegExp(
    ABILITY_SCORE_WITH_MOD_RE_STR + '\\s*' +
    ABILITY_SCORE_WITH_MOD_RE_STR.replace(/str/g, 'dex') + '\\s*' +
    ABILITY_SCORE_WITH_MOD_RE_STR.replace(/str/g, 'con') + '\\s*' +
    ABILITY_SCORE_WITH_MOD_RE_STR.replace(/str/g, 'int') + '\\s*' +
    ABILITY_SCORE_WITH_MOD_RE_STR.replace(/str/g, 'wis') + '\\s*' +
    ABILITY_SCORE_WITH_MOD_RE_STR.replace(/str/g, 'cha') + '\\s*',
    'ims'
);

const SAVE_BODY_RE = /saving\s*throws\s*((str|dex|con|int|wis|cha)\s*([+−-]\d+)?\s*,?\s*)+/ims;
const SAVE_RE_STR = String.raw`(?<ability>str|dex|con|int|wis|cha)\s*([+−-]\d+)`;

function parseAbilityScores (text) {
    let result = {
        'abilities': {}
    };

    let match = ABILITY_SCORE_LINE_RE.exec(text);
    if (match) {
        result['abilities'] = {
            'str': {'value': Number(match.groups['str'])},
            'dex': {'value': Number(match.groups['dex'])},
            'con': {'value': Number(match.groups['con'])},
            'int': {'value': Number(match.groups['int'])},
            'wis': {'value': Number(match.groups['wis'])},
            'cha': {'value': Number(match.groups['cha'])},
        }
    }

    // look for saving throws
    match = SAVE_BODY_RE.exec(text);
    if (match) {
        let allSaves = match[0];
        let saveRe = new RegExp(SAVE_RE_STR, 'imsg');
        while (match = saveRe.exec(allSaves)) {
            let abilityKey = match.groups['ability'].toLowerCase();
            if (!(abilityKey in result['abilities'])) {
                result['abilities'][abilityKey] = {};
            }
            result['abilities'][abilityKey]['proficient'] = 1;
        }
    }

    if (Object.keys(result['abilities']).length == 0)
        return {};
    return result;
}


function parseSenses (text) {
    let sense_re = /(?<sense>(Blindsight|Darkvision|Tremorsense|Truesight)\s*\d+\s*\w+\.?)/imsg;
    let match = null;
    let senses = '';
    while (match = sense_re.exec(text)) {
        if (senses.length > 0)
            senses += ', ';
        senses += match.groups['sense'].replace(/\s+/imsg, ' ');
    }

    if (senses.length == 0)
        return {};
    // belongs in the 'traits' object
    return {
        'senses': senses
    };
}


const CHALLENGE_RE = /Challenge\s*(?<cr>\d+(\s*\/\s*\d)?)/ims;

function parseChallengeRating (text) {
    let match = CHALLENGE_RE.exec(text);
    if (!match)
        return {};

    let crStr = match.groups['cr'].replace(/\s+/ims, '');
    let cr = null;
    if (crStr == '1/8') {
        cr = 0.125;
    }
    else if (crStr == '1/4') {
        cr = 0.25;
    }
    else if (crStr == '1/2') {
        cr = 0.5;
    }
    else
        cr = Number(crStr);

    return {
        'details': {
            'cr': cr,
        }
    };
}


const SKILL_BODY_RE = /skills\s*[:]?\s*((acrobatics|animal\s*handling|arcana|athletics|deception|history|insight|intimidation|investigation|medicine|nature|perception|performance|persuasion|religion|sleight\s*of\s*hand|stealth|survival)\s*([+−-]\d+),?\s*)+/ims;
const SKILL_RE_STR = String.raw`(?<skill>acrobatics|animal\s*handling|arcana|athletics|deception|history|insight|intimidation|investigation|medicine|nature|perception|performance|persuasion|religion|sleight\s*of\s*hand|stealth|survival)\s*(?<bonus>[+−-]\d+)`;

function parseSkills (text, actor, parsedSoFar) {
    let skillsList = SKILL_BODY_RE.exec(text);
    if (!skillsList) {
        return {};
    }
    skillsList = skillsList[0]; // switch to whole matched string

    let result = {
        'skills': {
        }
    };
    let skillRe = new RegExp(SKILL_RE_STR, 'imsg');
    let skillMatch = null;

    while (skillMatch = skillRe.exec(skillsList)) {
        let skillKey = SKILL_MAP[ skillMatch.groups['skill'].toLowerCase() ];
        let skillBonus = Number(skillMatch.groups['bonus'].replace('−', '-').replace(/\s+/, ''));
        let modKey = ABILITY_SKILL_MAP[skillKey];  // such as 'cha' for 'itm' (intimidation)
        let mod = modifierFromAbilityScore(parsedSoFar['abilities'][modKey]['value'] || actor.data.abilities[modKey].mod);
        let prof = profFromCr(parsedSoFar['details']['cr'] || actor.data.attributes.prof);

        if (mod < skillBonus && skillBonus < mod + prof)
            result['skills'][skillKey] = {'value': 0.5};
        else if (skillBonus == mod + prof)
            result['skills'][skillKey] = {'value': 1};
        else if (skillBonus == mod + prof * 2)
            result['skills'][skillKey] = {'value': 2};
    }

    if (Object.keys(result['skills']).length == 0) {
        return {};
    }
    return result;
}


function parseAttributes (text) {
    let result = {
        'attributes': {
            ...parseArmorClass(text),
            ...parseHitPoints(text),
            ...parseSpeed(text),
        }
    };
    if (Object.keys(result['attributes']).length == 0)
        return {};
    return result;
}


function parseTraits (text) {
    let result = {
        'traits': {
            ...parseSenses(text),
        }
    };
    if (Object.keys(result['traits']).length == 0)
        return {};
    return result;
}


export function parse (actor, data, text = null, allowGuesswork = true) {
    //console.log(actor);
    console.log(text);
    if (!text) {
        // TODO: Move this to a user-facing warning.
        console.warn('Empty actor description text; nothing to parse.');
        return {};
    }
    let updates = {
        ...parseAttributes(text),
        ...parseTraits(text),
        ...parseAbilityScores(text),
        //...parseLanguages(text), // TODO
        ...parseChallengeRating(text),
    };

    updates = {
        ...updates,
        ...parseSkills(text, actor, updates),
    };

    // Future: Maybe.  Parse action text out and create features using the itemUpdater.
    //         But, copied from the SRD PDF, the (lack of) formatting makes that _really_ messy.

    return updates;
}
