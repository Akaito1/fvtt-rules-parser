// updater.js
import * as parser from './itemParser.js';

export async function update (app, data, text = null, allowGuesswork = false) {
    // ask parser for details
    //console.log(data);
    let actor = app.object.options.actor;
    let updates = {'data': parser.parse(actor, data, text, allowGuesswork)};
    //console.log('parsed updates:'); console.log(updates);

    // apply those changes
    // future: prompt first?
    let itemId = data.entity._id;
    let item = game.items.get(itemId);  // for editing a global item
    let itemEmbedder = null;

    // if the item couldn't be found globally, it's embedded in an actor
    if (!item) {
        //console.log('Item is embedded!');
        updates['_id'] = itemId;  // sheet data has the ID within whatever scope the item exists in
        itemEmbedder = app.object.options.actor; // an actor definitely owns the item
        // but, we may actually be on an actor's token
        if ('token' in Object.keys(itemEmbedder.options)) {
            itemEmbedder = itemEmbedder.options.token;
        }
    }

    /*
    console.log('~*~ NEW TESTS ~*~');
    // options is empty for stand-alone items
    console.log('app.object looks like:'); console.log(app.object);
    // options has an actor for actor-embedded items
    console.log('app.object.options.actor looks like:'); console.log(app.object.options.actor);
    console.log('app.object.options.actor.options.token looks like:'); console.log(app.object.options.actor.options.token);
    //*/

    // If damage parts already exist, Foundry ignores our update.
    // So first remove all parts, then do the expected update.
    if ('damage' in updates['data'] && data.data.damage.parts.length > 0) {
        if (itemEmbedder)
            await itemEmbedder.updateOwnedItem({'_id': itemId, 'data': {'damage': {'parts': []}}});
        else
            await item.update({'data': {'damage': {'parts': []}}});
    }

    if (itemEmbedder) {
        //console.log(itemEmbedder);
        await itemEmbedder.updateOwnedItem(updates);
    }
    else
        await item.update(updates);
}
