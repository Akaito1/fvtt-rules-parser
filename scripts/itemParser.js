const ABILITY_MAP = {
    'strength': 'str',
    'dexterity': 'dex',
    'constitution': 'con',
    'intelligence': 'int',
    'wisdom': 'wis',
    'charisma': 'cha',
};

const ACTIVATION_MAP = {
    'action': 'action',
    'bonus action': 'bonus',
    'crew action': 'crew',
    'day': 'day',                 'days': 'day',
    'hour': 'hour',               'hours': 'hour',
    'legendary action': 'action', 'legendary actions': 'action',
    'minute': 'minute',           'minutes': 'minute',
    'none': 'none',  // What would specify this in text, if anything?
    'reaction': 'reaction',
    'special': 'special',
    'uses lair action': 'lair',  // What would this look like in text?
};

const DURATION_MAP = {
    'day': 'day',                 'days': 'day',
    'hour': 'hour',               'hours': 'hour',
    'instant': 'inst',            'instantaneous': 'inst',
    'minute': 'minute',           'minutes': 'minute',
    'month': 'month',             'months': 'month',
    'permanent': 'perm',
    'round': 'round',             'rounds': 'round',
    'special': 'spec',
    'turn': 'turn',               'turns': 'turn',
    'year': 'year',               'years': 'year',
};

const MAGIC_SCHOOL_MAP = {
    'abjuration': 'abj',
    'conjuration': 'con',
    'divination': 'div',
    'divination': 'div',
    'enchantment': 'enc',
    'evocation': 'evo',
    'illusion': 'ill',
    'necromancy': 'nec',
    'transmutation': 'trs',
};


// TODO: Support localized text in regex searches.

const DAMAGE_RE = /\(?(?<roll>\d+d\d+(\s*[+-]\s*[\dd]+)?)\)?\s+(?<type>\w+)\s+damage/i;
const DAMAGE_FLAT_RE = /(?<roll>\d+)\s+(?<type>\w+)\s+damage/i;

// Action types
const MSAK_RE = /make\s*a\s*melee\s*spell\s*attack/i;
const RSAK_RE = /make\s*a\s*ranged\s*spell\s*attack/i;
const MWAK_RE = /melee\s+weapon\s+attack.*:.*(?<hit>[+-]\d+)\s+to\s+hit/i;
const RWAK_RE = /ranged\s+weapon\s+attack.*:.*(?<hit>[+-]\d+)\s+to\s+hit/i;

// Casting time
//const CASTING_TIME_RE = /casting\s*time\s*:?\s*(?<amount>\d+)\s*(?<unit>\w+)/i;
const CASTING_TIME_RE = /casting\s*time\s*:?\s*(?<amount>\d+)\s*(?<unit>(actions?|bonus actions?|crew actions?|days?|hours?|legendary actions?|minutes?|reactions?|special|lair actions?))/ims;

// Duration
const DURATION_RE = /duration\s*:?\s*(?<amount>\d+)?\s*(?<unit>(days?|hours?|instant|instantaneous|minutes?|months?|permanent|rounds?|special|turns?|years?))/ims;

// misc.
const SPELL_LEVEL_SCHOOL_RE = /(?<level>\d+)\w+\s*-?\s*level\s*(?<school>(abjuration|conjuration|divination|enchantment|evocation|illusion|necromancy|transmutation))/ims;
const SPELL_CANTRIP_SCHOOL_RE = /(?<school>(abjuration|conjuration|divination|enchantment|evocation|illusion|necromancy|transmutation))\s*cantrip/ims;
// these next two feel awfully permissive, but they match one source I was testing with that didn't have much else to go on
const SPELL_LEVEL_RE = /level\s+(?<level>(cantrip|\d+))/ims;
const SPELL_SCHOOL_RE = /school\s+(?<school>(abjuration|conjuration|divination|enchantment|evocation|illusion|necromancy|transmutation))/ims;

// Range
const WEAPON_REACH_RE = /reach\s+(?<reach>\d+)\s+ft/i;
const WEAPON_RANGE_RE = /range\s+(?<normal>\d+)\s*\/\s*(?<long>\d+)\s+ft/i;

// Saving throw
const FLAT_SAVING_THROW = /DC\s*(?<dc>\d+)\s*(?<ability>\w+)\s*saving\s*throw/i;
const SPELL_SAVING_THROW = /make\s*a\s*(?<ability>\w+)\s*saving\s*throw/i;

// Spell scaling
const CANTRIP_LEVEL_SCALING = /increases\s+by\s+(?<dice>\d+d\d+)\s+when\s+you\s+reach\s+5th\s+level/i;
const SPELL_LEVEL_SCALING = /increases\s+by\s+(?<dice>\d+d\d+)\s+for\s+each\s+slot\s+level/i;

// Targeting
const ONE_TARGET_RE = /one\s+target/i;
const RADIUS_TARGET_RE = /each\s*creature\s*in\s*a\s*(?<size>\d+)-f(oo)?t-radius\s*(?<shape>\w+)/i;


function parseActivation (text, allowGuesswork) {
    let match;
    if (match = CASTING_TIME_RE.exec(text)) {
        return {
            'activation': {
                'type': ACTIVATION_MAP[match.groups['unit'].toLowerCase()],
                'cost': Number(match.groups['amount']),
            }
        };
    }
    // fall back to a very common setup to make other fields usable
    if (allowGuesswork)
        return {
            'activation': {
                'type': 'action',
                'cost': 1
            },
        };
    return {};
}


function parseActionType (actor, text, allowGuesswork) {
    let match;
    let result = null;
    if (MSAK_RE.test(text)) {
        return { 'actionType': 'msak' };
    }
    else if (RSAK_RE.test(text)) {
        return { 'actionType': 'rsak' };
    }
    else if (match = MWAK_RE.exec(text)) {
        result = {
            'actionType': 'mwak',
            'duration': {
                'value': null,
                'units': 'inst',  // instantaneous
            }
        };
    }
    else if (match = RWAK_RE.exec(text)) {
        result = {
            'actionType': 'rwak',
            'duration': {
                'value': null,
                'units': 'inst',  // instantaneous
            }
        };
    }

    if (result !== null) {
        if (actor !== null) {
            // Check if the attack is based on something other than just the default (strength).
            if (result['actionType'] == 'mwak' || result['actionType'] == 'rwak') {
                let bonus = Number(match.groups['hit'])
                let prof = actor.data.data.attributes.prof;
                // ordered list of stats that seem reasonable-ish to me
                let stats = ['str','dex','int','wis','cha','con'];
                for (let i = 0 ; i < stats.length; ++i) {
                    let mod = actor.data.data.abilities[stats[i]]['mod'];
                    if (bonus == prof + mod) {
                        result['ability'] = stats[i];
                        result['attackBonus'] = '';
                        break;
                    }
                }
                // If the to-hit bonus is some weird number, as in some very low-CR stat blocks,
                // just fall-back to Strength and give it an arbitrary bonus to make up the difference.
                if (allowGuesswork && !('ability' in result)) {
                    result['ability'] = 'str';
                    result['attackBonus'] = bonus - (prof + actor.data.data.abilities['str']['mod']);
                }
            }
        }
        return result;
    }
    return {};
}


function parseDamageSpell (text, allowGuesswork) {
    let matchFound = false;
    let re = new RegExp(DAMAGE_RE.source, 'ig');
    let match = null;
    let result = {
        'damage': {
            'parts': [],
            'versatile': ''
        },
    };
    while (true) {
        match = re.exec(text);
        if (!match)
            break;
        // TODO: figure out difference between two damage types
        //       and damage happening at two separate times.
        //       For now, assume two totally separate things.
        if (result['damage']['parts'].length == 0) {
            matchFound = true;
            result['damage']['parts'].push([match.groups['roll'], match.groups['type']]);
        }
        else {
            matchFound = true;
            result['damage']['versatile'] = match.groups['roll'];
            // could we improve flavor to include more of the description?
            result['chatFlavor'] = match.groups['type'];
            break;
        }
    }
    if (matchFound)
        return result;

    // Flat damage can be seen on some very low CR creatures.
    if (match = DAMAGE_FLAT_RE.exec(text)) {
        result['damage']['parts'].push([match.groups['roll'], match.groups['type']]);
        return result;
    }

    return {};
}


function parseDuration (text) {
    let match;
    if (match = DURATION_RE.exec(text)) {
        return {
            'duration': {
                'value': Number(match.groups['amount']) || null,
                'units': DURATION_MAP[match.groups['unit'].toLowerCase()],
            }
        };
    }
    return {};
}


// TODO
function parsePreparation (allowGuesswork) {
    if (allowGuesswork)
        return {
            'preparation': {
                'mode': 'prepared',
                'prepared': false
            }
        };
    return {};
}


function parseRange (text) {
    let match;
    if (match = WEAPON_REACH_RE.exec(text)) {
        return {
            'range': {
                'value': match.groups['reach'],
                'long': null,
                'units': 'ft',  // TODO
            },
            'weaponType': 'natural',  // or guess at 'simpleM'?
        };
    }
    else if (match = WEAPON_RANGE_RE.exec(text)) {
        return {
            'range': {
                'value': match.groups['normal'],
                'long': match.groups['long'],
                'units': 'ft',  // TODO
            },
            'weaponType': 'natural',  // or guess at 'simpleR'?
        };
    }
    return {};
}


function parseSavingThrow (data, text) {
    let match;
    if (match = SPELL_SAVING_THROW.exec(text)) {
        return {
            'actionType': 'save',
            'save': {
                'ability': ABILITY_MAP[match.groups['ability'].toLowerCase()],
                'dc': null,
                'scaling': 'spell',
            }
        };
    }
    else if (match = FLAT_SAVING_THROW.exec(text)) {
        let result = {
            'save': {
                'ability': ABILITY_MAP[match.groups['ability'].toLowerCase()],
                'dc': Number(match.groups['dc']),
                'scaling': 'flat',
                //'value': '',
            }
        };
        if (data['actionType'] == null || data['actionType'] == '') {
            result['actionType'] = 'save';
        }
        return result;
    }
    return {};
}


function parseSpellScaling (text) {
    let match;
    if (match = SPELL_LEVEL_SCALING.exec(text)) {
        return {
            'scaling': {
                'mode': 'level',
                'formula': match.groups['dice'],
            }
        };
    }
    else if (match = CANTRIP_LEVEL_SCALING.exec(text)) {
        return {
            'scaling': {
                'mode': 'cantrip',
                'formula': match.groups['dice'],
            }
        };
    }
    return {};
}


function parseSpellSchoolLevel (text) {
    let match;
    if (match = SPELL_LEVEL_SCHOOL_RE.exec(text)) {
        return {
            'school': MAGIC_SCHOOL_MAP[match.groups['school'].toLowerCase()],
            'level': Number(match.groups['level']),
        };
    }
    else if (match = SPELL_CANTRIP_SCHOOL_RE.exec(text)) {
        return {
            'school': MAGIC_SCHOOL_MAP[match.groups['school'].toLowerCase()],
            'level': 0,
        };
    }
    else {
        let result = {};
        if (match = SPELL_LEVEL_RE.exec(text)) {
            result['level'] = match.groups['level'].toLowerCase() == 'cantrip' ? 0 : Number(match.groups['level']);
        }
        if (match = SPELL_SCHOOL_RE.exec(text)) {
            result['school'] = MAGIC_SCHOOL_MAP[match.groups['school'].toLowerCase()];
        }
        if (Object.keys(result).length !== 0)
            return result;
    }
    return {};
}


function parseTarget (text) {
    let match;
    if (ONE_TARGET_RE.test(text)) {
        return {
            'target': {
                'value': 1,
                'units': '',
                'type': 'creature',
            }
        };
    }
    else if (match = RADIUS_TARGET_RE.exec(text)) {
        return {
            'target': {
                'value': Number(match.groups['size']),
                'units': 'ft',
                'type': match.groups['shape'].toLowerCase(),
            }
        };
    }
    return {};
}


export function parse (actor, data, text = null, allowGuesswork = true) {
    console.log(actor);
    if (!text)
        text = data.data.description.value;
    let updates = {
        ...parseActivation(text, allowGuesswork),
        ...parseActionType(actor, text, allowGuesswork),
        ...parseDamageSpell(text),
        ...parseDuration(text),
        ...parsePreparation(allowGuesswork),
        ...parseRange(text),
        ...parseSavingThrow(data, text),  // must be done after action type
        ...parseSpellScaling(text),
        ...parseSpellSchoolLevel(text),
        ...parseTarget(text),
    };

    return updates;
}
