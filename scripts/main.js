// main.js
import * as itemUpdater from './itemUpdater.js';
import * as morePrompt from './moreTextPrompt.js';


//////////////////////////////////////
// User opens a standard item sheet //
//////////////////////////////////////
// To discover which hooks to use, run `CONFIG.debug.hooks = true` in Foundry's console, then open the sheet of interest.

Hooks.on('renderItemSheet5e', (app, html, data) => {
    //console.log('app:'); console.log(app);
    //console.log('html:'); console.log(html);
    //console.log('data:'); console.log(data);

    // Disallow editing items on tokens, because we're not sure how to yet.
    if (app.object.options.actor?.options.token)
        return;

    // Parse Desc. button
    // TODO: Stop using an id.
    let parseDescButton = $(
        `<button id="rules-parser-parse-rules-button" class="${game.system.id}">Parse Desc.</button>`
    );
    parseDescButton.on('click', async (e) => itemUpdater.update(app, data, null, true));

    // Parse More button
    // TODO: Stop using an id.
    let parseMoreButton = $(
        `<button id="rules-parser-parse-more-rules-button">Parse More</button>`
    );
    parseMoreButton.on('click', async (e) => {
        let prompt = new morePrompt.MoreTextPrompt();
        prompt.setApp(app);
        prompt.setItemData(data);
        prompt.render(true);
    });

    $(html).find('.item-properties').prepend(parseMoreButton);
    $(html).find('.item-properties').prepend(parseDescButton);
});


///////////////////////////////////////
// User opens a standard actor sheet //
///////////////////////////////////////

Hooks.on('renderActorSheet5eNPC', (app, html, data) => {
    //console.log('app:'); console.log(app);
    //console.log('html:'); console.log(html);
    //console.log('data:'); console.log(data);

    // Parse More button
    // TODO: Stop using an id.
    let parseMoreButton = $(
        `<button id="rules-parser-parse-more-rules-button">Parse More</button>`
    );
    parseMoreButton.on('click', async (e) => {
        let prompt = new morePrompt.MoreTextPrompt();
        prompt.setApp(app);
        prompt.setActorData(data);
        prompt.render(true);
    });

    $(html).find('.configure-flags').parent().append(parseMoreButton);
});
